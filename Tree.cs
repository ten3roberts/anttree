﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

// Wrapper for the root of a red black tree to allow deletion of singleton
class RBTree<K, V> where K : IComparable<K>
{
    RBTreeNode root = null;
    int nodeCount = 0;

    // Returns true if the tree is empty, I.e; root is null
    public bool Empty()
    {
        return root == null;
    }

    // Inserts a key and value into the tree
    public void Insert(K newKey, V newValue)
    {
        // Handle empty tree
        if (root == null)
        {
            root = new RBTreeNode(newKey, newValue);
            nodeCount = 1;
        }
        else
        {
            var backtrace = root.InsertBalanced(newKey, newValue);
            nodeCount += backtrace.newNode ? 1 : 0;
        }

        // Color root black
        root.red = false;
    }

    // Removes a key and value from tree
    // Returns true if key was found
    // Returns false if key was not found or tree was empty
    public bool Remove(K key)
    {
        if (root == null) return false;
        RBTreeNode.RemoveBacktrace backtrace = root.RemoveWithRoot(root, key);
        // Make root black
        root.red = false;
        root = backtrace.replace;

        nodeCount -= backtrace.found ? 1 : 0;

        return backtrace.found;
    }

    public void PrintBlackHeights()
    {
        if (root != null)
            root.PrintBlackHeights();
        Console.WriteLine();
    }

    public void Print()
    {
        if (root != null)
            root.Print();
    }

    // Returns an iterator that traverses the tree in order
    public IEnumerable<(K, V)> Traverse()
    {
        if (root != null)
            foreach (var node in root.Traverse())
            {
                yield return node;
            }
        else
            yield break;
    }

    // Returns the smallest key in the tree
    public K GetSmallest()
    {
        if (root == null)
        {
            return default;
        }
        return root.GetSmallest().Key;
    }

    // Returns the largest key in the tree
    public K GetLargest()
    {
        if (root == null)
        {
            return default;
        }
        return root.GetLargest().Key;
    }


    // Returns the total number of nodes in the tree
    public int NodeCount()
    {
        return nodeCount;
    }

    // Returns the max depth of a tree
    public int MaxDepth()
    {
        return root.MaxDepth();
    }

    // Returns what would be the optimal depth for the tree
    public int OptimalDepth()
    {
        return root.OptimalDepth();
    }

    public void PrintBlackHeights(int startHeight = 0)
    {
        root.PrintBlackHeights();
    }

    // Returns true and fills the result with value if key exists in tree
    public bool TryFind(K key, out V result)
    {
        if (root == null)
        {
            result = default;
            return false;
        }

        return root.TryFind(key, out result);
    }

    // Returns true if key exists in key
    public bool Exists(K key)
    {
        return TryFind(key, out _);
    }

    // Returns the closest key
    public static string FindClosest(RBTree<string, V> tree, string key)
    {
        if (tree.root == null)
            return default;

        return RBTree<string, V>.RBTreeNode.FindClosest(tree.root, key);
    }

    // Red black tree associative container of K -> V
    class RBTreeNode
    {
        public struct InsertBacktrace
        {
            public InsertBacktrace(int depth, int direction, bool newNode)
            {
                this.depth = depth;
                this.direction = direction;
                this.newNode = newNode;
            }
            public int depth;
            public int direction;
            public bool newNode;
        }

        public struct RemoveBacktrace
        {
            public RemoveBacktrace(RBTreeNode replace, bool rebalance, bool found = true)
            {
                this.replace = replace;
                this.rebalance = rebalance;
                this.found = found;
            }
            public RBTreeNode replace;
            // Rebalance is triggered when black height changes
            public bool rebalance;
            public bool found;
        }

        public RBTreeNode(K key, V value)
        {
            this.value = value;
            this.key = key;
            red = true;
            left = null;
            right = null;
        }

        // Inserts a value into the tree with a key
        // Replaces a value if it already exists
        public InsertBacktrace InsertBalanced(K newKey, V newValue)
        {
            int cmp = newKey.CompareTo(key);
            InsertBacktrace backtrace = new InsertBacktrace();
            if (cmp < 0)
            {
                bool newNode = false;
                if (left == null)
                {
                    left = new RBTreeNode(newKey, default(V));
                    newNode = true;
                }

                var ret = left.InsertBalanced(newKey, newValue);
                backtrace.depth = ret.depth + 1;
                backtrace.direction = (ret.direction << 1) + 0;
                backtrace.newNode = ret.newNode || newNode;
            }
            else if (cmp > 0)
            {
                bool newNode = false;
                if (right == null)
                {
                    right = new RBTreeNode(newKey, default(V));
                    newNode = true;
                }

                var ret = right.InsertBalanced(newKey, newValue);
                backtrace.depth = ret.depth + 1;
                backtrace.direction = (ret.direction << 1) + 1;
                backtrace.newNode = ret.newNode || newNode;
            }
            // New node added
            else
            {
                value = newValue;
                key = newKey;
                return new InsertBacktrace(0, 0, true);
            }

            // Not reached grandparent yet
            if (backtrace.depth == 2)
                return RebalanceInsert(this, backtrace);
            else
                return backtrace;

        }

        static InsertBacktrace RebalanceInsert(RBTreeNode grandparent, InsertBacktrace backtrace)
        {
            // Get parent of new node from grandparent and backtrace direction
            RBTreeNode parent =
                    (backtrace.direction & 1) == 0 ?
                    grandparent.left : grandparent.right;

            // The new node
            RBTreeNode newNode =
                    (backtrace.direction & 2) == 0 ?
                    parent.left : parent.right;

            // Sibling of parent node
            RBTreeNode sibling =
                    (backtrace.direction & 1) == 0 ?
                    grandparent.right : grandparent.left;

            // No property is violated, return and don't rebalance
            if (IsBlack(parent))
                return backtrace;

            if (IsRed(sibling))
            {
                // Recolor
                sibling.red = false;
                parent.red = false;
                grandparent.red = true;
                return new InsertBacktrace(0, 0, backtrace.newNode);
            }
            // Sibling is black, rotate and recolor
            // Parent and new node are both red
            else
            {
                // Make parent and new node a line from gp
                if (parent == grandparent.right && newNode == parent.left)
                    parent.RotateRight();
                else if (parent == grandparent.left && newNode == parent.right)
                    parent.RotateLeft();

                // If rotated, parent is now swapped with new node
                // Rotate parent node up so that 1 gets added to siblings black height
                // Recolor parent of new node to black
                parent.SwapColor(grandparent);
                // Grand parent will then move down and be sibling to new node
                grandparent.red = true;
                if (parent == grandparent.left)
                {
                    grandparent.RotateRight();
                }
                else if (parent == grandparent.right)
                {
                    grandparent.RotateLeft();
                }
                // Do rebalance with grandparent since parent of grandparent could be red
                return backtrace;
            }
        }

        public RemoveBacktrace RemoveWithRoot(RBTreeNode root, K key)
        {
            int cmp = key.CompareTo(this.key);
            if (cmp == 0)
            {
                return RemoveSelf(root);
            }
            else if (cmp < 0 && left != null)
            {
                var backtrace = left.RemoveWithRoot(root, key);
                left = backtrace.replace;
                // Rebalance may be needed again
                if (backtrace.rebalance && RebalanceRemove(right))
                {
                    return new RemoveBacktrace(this, true, backtrace.found);
                }
                return new RemoveBacktrace(this, false, backtrace.found);
            }
            else if (cmp > 0 && right != null)
            {
                var backtrace = right.RemoveWithRoot(root, key);
                right = backtrace.replace;
                // Rebalance may be needed again
                if (backtrace.rebalance && RebalanceRemove(left))
                {
                    return new RemoveBacktrace(this, true, backtrace.found);
                }

                return new RemoveBacktrace(this, false, backtrace.found);
            }
            else
            {
                return new RemoveBacktrace(this, false, false);
            }
        }

        // Removes self and recurses to zsuccessor if 2 children
        RemoveBacktrace RemoveSelf(RBTreeNode root)
        {
            // Internal node with 2 children
            if (right != null && left != null)
            {
                // Replace with in order successor, the left most node in the right subtree
                RBTreeNode succ = right.GetSmallest();

                bool oldRed = red;
                K oldKey = key;
                V oldVal = value;
                key = succ.key;
                value = succ.value;


                // Remove the in order successor
                RemoveBacktrace backtrace = right.RemoveWithRoot(root, key);

                right = backtrace.replace;

                if (backtrace.rebalance && RebalanceRemove(left))
                {
                    return new RemoveBacktrace(this, true, backtrace.found);
                }
                return new RemoveBacktrace(this, false, backtrace.found);
            }

            else if (left != null)
            {
                // Either current node or child are red
                // Black height doesn't change, no rebalance
                if (red || left.red)
                {
                    left.red = false;
                    return new RemoveBacktrace(left, false);
                }
                else
                {
                    // Double black
                    return new RemoveBacktrace(left, true);
                }
            }
            else if (right != null)
            {
                // Either current node or child are red
                // Black height doesn't change, no rebalance
                if (red || right.red)
                {
                    right.red = false;
                    // Double black
                    return new RemoveBacktrace(right, false);
                }
                else
                {
                    return new RemoveBacktrace(right, true);
                }
            }
            // Current node is a leaf node
            else
            {
                // A red node is removed, black height doesn't change
                // or
                // A black node is removed, black height changes
                return new RemoveBacktrace(null, IsBlack(this));
            }
        }

        public RBTreeNode GetSmallest()
        {
            RBTreeNode smallest = this;
            while (smallest.left != null)
            {
                smallest = smallest.left;
            }
            return smallest;
        }

        public RBTreeNode GetLargest()
        {
            RBTreeNode largest = this;
            while (largest.right != null)
            {
                largest = largest.right;
            }
            return largest;
        }

        // Remove is node to be removed
        // `this` is parent node
        // Sibling is sibling of removed node
        // Fixes the double black
        bool RebalanceRemove(RBTreeNode sibling)
        {
            // Sibling is red, rotate
            if (IsRed(sibling))
            {
                // Recolor
                red = true;
                sibling.red = false;
                // Rotate
                RBTreeNode newSibling = null;
                if (sibling == left)
                {
                    newSibling = sibling.right;
                    RotateRight();
                }
                else if (sibling == right)
                {
                    newSibling = sibling.left;
                    RotateLeft();
                }
                sibling = newSibling;
                // Do black sibling case
                return RebalanceRemove(sibling);
            }

            // Sibling is right
            Debug.Assert(IsBlack(sibling));

            // Atleast one of siblings children is red
            RBTreeNode redChild = null;
            bool bothRed = false;

            // Find one or more red children
            if (sibling != null && IsRed(sibling.left))
                redChild = sibling.left;
            if (sibling != null && IsRed(sibling.right))
            {
                if (redChild != null) bothRed = true;
                redChild = sibling.right;
            }

            // Atleast one red child
            if (redChild != null)
            {
                RemoveCaseRedChild(sibling, redChild, bothRed);
                return false;
            }
            // No red children of sibling
            else
            {
                return RemoveCaseBlack(sibling);
            }
        }


        // Case where sibling is black and atleast one child of sibling is red
        // redChild is not null
        void RemoveCaseRedChild(RBTreeNode sibling, RBTreeNode redChild, bool bothRed)
        {
            sibling.SwapColor(redChild);
            //red = false;
            //redChild.red = false;

            // Case depending on positions of red child of sibling
            // sibling is right and red is right child of sibling (or both, but red was last)
            // Right Right
            if (sibling == right && redChild == sibling.right)
            {
                RotateLeft();
            }

            // Right Left
            else if (sibling == right && redChild == sibling.left)
            {

                sibling.RotateRight();
                RotateLeft();
            }

            // Left Left case
            else if (sibling == left && redChild == sibling.left)
            {
                RotateRight();
            }
            // Left Right
            else if (sibling == left && redChild == sibling.right)
            {
                sibling.RotateLeft();
                RotateRight();
            }
        }

        // Case where sibling is black and both children of sibling is black
        bool RemoveCaseBlack(RBTreeNode sibling)
        {
            // Recolor sibling
            if (sibling != null)
                sibling.red = true;
            // Current node is red, absorb the double black
            if (IsRed(this))
            {
                red = false;
                return false;
            }
            // Parent is red, no need to rebalance parent
            else
            {
                return true;
            }
        }

        // Attempts to find a key in the tree
        // Writes value output to result if successful
        // Returns true on success

        public bool TryFind(K key, out V result)
        {
            int cmp = key.CompareTo(this.key);
            // Key was found
            if (cmp == 0)
            {
                result = value;
                return true;
            }

            // Look left
            if (cmp < 0 && left != null)
                return left.TryFind(key, out result);
            // Look right
            else if (cmp > 0 && right != null)
                return right.TryFind(key, out result);
            // Not found
            else
            {
                result = default(V);
                return false;
            }
        }
        public static string FindClosest(RBTree<string, V>.RBTreeNode node, string key)
        {
            // Partial match
            if (node.key.StartsWith(key))
                return node.key;

            int cmp = key.CompareTo(node.key);
            // Look left
            if (cmp < 0 && node.left != null)
                return FindClosest(node.left, key);
            // Look right
            else if (cmp > 0 && node.right != null)
                return FindClosest(node.right, key);

            // Match or closest
            else
            {
                return null;
            }
        }

        public RBTreeNode FindNode(K key)
        {
            int cmp = key.CompareTo(this.key);
            // Key was found
            if (cmp == 0)
            {
                return this;
            }

            // Look left
            if (cmp < 0 && left != null)
                return left.FindNode(key);
            // Look right
            else if (cmp > 0 && right != null)
                return right.FindNode(key);
            // Not found
            else
            { return null; }
        }

        public void Print()
        {
            if (left != null)
                left.Print();

            if (right != null)
                right.Print();
        }

        // Returns an iterator that traverses the tree
        public IEnumerable<(K, V)> Traverse()
        {
            // Yield all elements less than self
            if (left != null)
                foreach (var node in left.Traverse())
                {
                    yield return node;
                }

            // Yield value
            yield return (key, value);

            // Yield all values less than self
            if (right != null)
                foreach (var node in right.Traverse())
                {
                    yield return node;
                }
        }

        // Returns the total number of nodes in the tree
        public int NodeCount()
        {
            int count = 1;
            if (left != null) count += left.NodeCount();
            if (right != null) count += right.NodeCount();
            return count;
        }

        // Returns the max depth of a tree
        public int MaxDepth()
        {
            int leftDepth = 0;
            int rightDepth = 0;
            if (left != null)
                leftDepth = left.MaxDepth();
            if (right != null)
                rightDepth = right.MaxDepth();

            return Math.Max(leftDepth, rightDepth) + 1;
        }

        // Returns what would be the optimal depth for the tree
        public int OptimalDepth()
        {
            int nodeCount = NodeCount();
            return (int)Math.Floor(Math.Log2(nodeCount)) + 1;
        }

        public void PrintBlackHeights(int startHeight = 0)
        {
            if (IsBlack(this)) startHeight++;
            if (left == null && right == null)
            {
                Console.Write("key: " + key + " - " + startHeight + ", ");
            }
            if (left != null) left.PrintBlackHeights(startHeight);
            if (right != null) right.PrintBlackHeights(startHeight);
        }

        // Swap nodes value, key and color
        // Does not change children
        void SwapNode(RBTreeNode other)
        {
            K oldK = key;
            V oldV = value;
            bool oldRed = red;

            value = other.value;
            key = other.key;
            red = other.red;

            other.key = oldK;
            other.value = oldV;
            other.red = oldRed;
        }

        void SwapColor(RBTreeNode other)
        {
            bool oldRed = red;
            red = other.red;
            other.red = oldRed;
        }

        void RotateLeft()
        {
            // Swap self and right values and keys
            SwapNode(right);

            // Move subchildren around
            var oldRight = right;

            right = right.right;

            oldRight.right = oldRight.left;
            oldRight.left = left;

            left = oldRight;
        }

        void RotateRight()
        {
            // Swap self and left values and keys
            SwapNode(left);

            // Move subchildren around
            var oldLeft = left;

            left = left.left;


            oldLeft.left = oldLeft.right;
            oldLeft.right = right;

            right = oldLeft;
        }

        // Finds the in order succesor of key
        // Always start from root
        RBTreeNode InOrderSucc(K key)
        {
            int cmp = key.CompareTo(this.key);

            // Current node larger. To find successor, go left unless it isn't the key
            if (cmp < 0)
            {
                if (left != null && key.CompareTo(left.key) != 0)
                    return left.InOrderSucc(key);
                else
                    return this;
            }
            // Current node is smaller, look right
            // Return current node if there is no left child
            else if (cmp >= 0)
            {
                if (right != null)
                    return right.InOrderSucc(key);
                else
                    return null;
            }
            return null;
        }


        // Returns true if node is red and not nil
        static bool IsRed(RBTreeNode node)
        {
            return node == null ? false : node.red;
        }

        // Returns true if node is black or nil
        static bool IsBlack(RBTreeNode node)
        {
            return node == null ? true : !node.red;
        }

        public override string ToString()
        {
            return $"key: {key}, value: {value}";
        }

        public K Key
        {
            get
            {
                return key;
            }
        }

        K key;
        V value;
        public bool red;
        RBTreeNode left;
        RBTreeNode right;
    }
}
