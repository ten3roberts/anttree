﻿using System;
using System.Collections.Generic;
using System.IO;

class Program
{
    RBTree<string, Ant> ants;
    bool shouldClose;
    // Names are always stored normalized
    List<string> availRandNames;
    Shell shell;

    const string banner =
@"     _          _  _____
    / \   _ __ | ||_   _| __ ___  ___
   / _ \ | '_ \| __|| || '__/ _ \/ _ \
  / ___ \| | | | |_ | || | |  __/  __/
 /_/   \_\_| |_|\__||_||_|  \___|\___|

 Interact with the tree by giving commands. ""command arg1 arg2 ...""
 Arguments are separated by space. Use quotes ' or "" for argument containing space
 Use `help` for a list of commands and their usage
_^";

    // Prints a nicely formatted banner
    void PrintBanner()
    {
        int width = Console.WindowWidth;
        foreach (var line in banner.Split('\n'))
        {
            string toPrint = "|" + line.TrimEnd();
            // Make string no longer than the screen
            toPrint = toPrint.Substring(0, Math.Min(toPrint.Length, width - 1));

            // Repeat second to last character if line ends in ^
            if (toPrint.EndsWith('^'))
            {
                toPrint = toPrint[0..(toPrint.Length - 2)].PadRight(width - 1, toPrint[toPrint.Length - 2]);
            }
            else
            {
                toPrint = toPrint.PadRight(width - 1);
            }

            Console.ForegroundColor = System.ConsoleColor.DarkCyan;
            Console.WriteLine(toPrint);
        }
    }


    Program()
    {
        ants = new RBTree<string, Ant>();
        availRandNames = new List<string>();
        shell = new Shell();
        shouldClose = false;
    }

    static void Main(string[] args)
    {
        new Program().Run();
    }

    CommandResult AddAntCommand(string[] args)
    {
        if (args.Length != 2)
            return CommandResult.IncorrectUsage;

        string name = Ant.NormalizeName(args[0]);
        int legCount = 0;

        if (!int.TryParse(args[1], out legCount))
        {
            Utils.Error("Malformed integer in second argument");
            return CommandResult.Error;
        }

        if (ants.Exists(name))
        {
            Utils.Error($"Ant `{name}` already exists");
            return CommandResult.Error;
        }

        return AddAnt(name, legCount) ? CommandResult.Ok : CommandResult.Error;
    }

    // Adds an ant
    bool AddAnt(string name, int legCount)
    {
        Ant ant;

        try
        {
            ant = new Ant(name, legCount);
        }
        catch (Ant.InvalidNameException e)
        {
            Utils.Error(e.Message);
            return false;
        }

        Utils.PrintColoredLn($"[Cyan]Added '{ant.Name}' with {ant.LegCount} legs");

        ants.Insert(ant.Name, ant);

        return true;
    }

    CommandResult RemoveAntCommand(string[] args)
    {
        if (args.Length != 1)
            return CommandResult.IncorrectUsage;

        string name = Ant.NormalizeName(args[0]);

        if (!RemoveAnt(name))
        {
            // Suggest the closest ant and try to removes again
            // Default to no since this command is destructive
            string suggestion = Utils.Suggest(ants, name, false);
            if (suggestion != null)
            {
                return RemoveAnt(suggestion) ? CommandResult.Ok : CommandResult.Error;
            }
            return CommandResult.Error;
        }
        else
        {
            return CommandResult.Ok;
        }
    }

    bool RemoveAnt(string name)
    {
        // Normalize name capitalization to ensure the search is case insensitive
        name = Ant.NormalizeName(name);

        // Try remove the ant from the tree
        if (!ants.Remove(name))
        {
            Utils.Error($"Ant '{name}' does not exist");



            return false;
        }

        // Add the removed ant's name to the available random names
        availRandNames.Add(name);
        Utils.PrintColoredLn($"[DarkCyan]Removed '{name}'");

        return true;
    }

    // Lists an ant
    // If there is no argument given it will list all ants
    // If there are arguments given it will concatenate them into one
    // It will then match an operator (<, >, =, <=, >=) and a number
    // It will then use that expression as a predicate matching the ants name
    CommandResult ListAnts(string[] args)
    {
        // Initializes a predicate function that matches everything
        Func<int, bool> predicate = (int legs) => true;

        // More than one argument given
        if (args.Length >= 1)
        {
            string predString = string.Concat(args);
            char predChar = predString[0];

            int predLegs = 0;

            // Get the index after the operator or a combination of them >= or even >>
            int lastCompareChar = predString.LastIndexOfAny(new[] { '=', '<', '>' });
            // Get only the integer
            string cleaned = predString[(lastCompareChar + 1)..];

            // Get the integer
            if (!int.TryParse(cleaned, out predLegs))
            {
                Utils.Error("Malformed integer in predicate");
                return CommandResult.Error;
            }

            // Remove the int from the operator string
            predString = predString[0..(lastCompareChar + 1)];

            // Match on the operators and generate a predicate string
            if (predString == "" || predString == "=")
                predicate = (int legs) => legs == predLegs;
            else if (predString == ">=")
                predicate = (int legs) => legs >= predLegs;
            else if (predString == "<=")
                predicate = (int legs) => legs <= predLegs;
            else if (predString == "<")
                predicate = (int legs) => legs < predLegs;
            else if (predString == ">")
                predicate = (int legs) => legs > predLegs;
            else
            {
                Utils.Error("Malformed predicate, use help for usage");
                return CommandResult.Error;
            }

        }

        // Get the largest name to pad correctly
        int longestName = 0;
        int index = 0;
        foreach (var (_, ant) in ants.Traverse())
        {
            if (ant.Name.Length > longestName)
                longestName = ant.Name.Length;

            index++;
        }
        int longestIndex = (int)Math.Log10(index) + 3;

        int listedAnts = 0;
        int totalAnts = 0;

        // Iterate the ants in the tree
        foreach (var (_, ant) in ants.Traverse())
        {
            totalAnts++;
            // Match with the predicate that was generated (true for all if note specified)
            if (!predicate(ant.LegCount))
                continue;

            // Print the ants index, name and legs with padding in column form
            string indexString = $"{listedAnts}: ";
            string legPadding = new string(' ', longestName - ant.Name.Length + 1);
            string indexPadding = new string(' ', longestIndex - indexString.Length);

            Utils.PrintColoredLn(indexString + indexPadding + ant.Name + ","
                + legPadding + ant.LegString());
            listedAnts++;
        }

        Utils.PrintColoredLn($"[Magenta]Listed {listedAnts}/{totalAnts} ants");

        return CommandResult.Ok;
    }

    CommandResult SearchAnt(string[] args)
    {
        if (args.Length != 1)
            return CommandResult.IncorrectUsage;

        Ant ant;
        // Get the name to search for as the first argument
        // Normalize capitalization to that of other ants
        string name = Ant.NormalizeName(args[0]);

        // Try lookup the name in tree
        if (!ants.TryFind(name, out ant))
        {
            // Ant does not exist
            Utils.Error($"Ant '{name}' does not exist");

            // Suggest the closest ant and search again
            string suggestion = Utils.Suggest(ants, name);
            if (suggestion != null)
            {
                return SearchAnt(new[] { suggestion });
            }
            return CommandResult.Error;
        }

        // Print the resulting ant
        Utils.PrintColoredLn(ant.Name + " has " + ant.LegString());
        return CommandResult.Ok;
    }

    // Clears the screen and redraws the banner
    CommandResult ClearScreen(string[] _args)
    {
        Console.Clear();
        PrintBanner();
        return CommandResult.Ok;
    }

    // Generate a random ant
    CommandResult GenerateAnt(string[] args)
    {
        // Default values
        int count = 1;
        int maxLegs = 100;
        int minLegs = 0;

        // Set count with first arg if it exists
        // Count
        if (args.Length >= 1 && !int.TryParse(args[0], out count))
        {
            Utils.Error("Malformed integer in third argument");
            return CommandResult.Error;
        }

        // Set minLegs with second arg if it exists
        // Min legs
        if (args.Length >= 2 && !int.TryParse(args[1], out minLegs))
        {
            Utils.Error("Malformed integer in first argument");
            return CommandResult.Error;
        }

        // Set maxLegs with third arg if it exists
        // Max legs
        if (args.Length >= 3 && !int.TryParse(args[2], out maxLegs))
        {
            Utils.Error("Malformed integer in second argument");
            return CommandResult.Error;
        }

        // Generate and add count ants
        for (int i = 0; i < count; i++)
        {
            // Get an available random name
            string name = TakeRandName();

            if (name == null)
            {
                Utils.PrintColoredLn("[Magenta]No more available random names");
                break;
            }

            int legs = new Random().Next(minLegs, maxLegs);

            // Use the user facing api for adding ants
            AddAnt(name, legs);
        }

        return CommandResult.Ok;
    }

    // Remove n ants
    CommandResult PurgeAnt(string[] args)
    {
        // Default to 1
        int count = 1;

        // If first arg is "all" or "a", purge all ants
        if (args.Length >= 1 && (args[0].ToLower() == "all" || args[0].ToLower() == "a"))
        {
            count = -1;
        }

        // Set count to first argument if it exists
        else if (args.Length >= 1 && !int.TryParse(args[0], out count))
        {
            Utils.Error("Malformed integer in second argument");
            return CommandResult.Error;
        }

        // Delete count ants
        for (int i = 0; i < count | count == -1; i++)
        {
            string name = ants.GetSmallest();
            if (name == null || !RemoveAnt(name))
            {
                Utils.PrintColoredLn("[Magenta]No more ants to remove");
                break;
            }
        }
        return CommandResult.Ok;
    }


    CommandResult Exit(string[] _args)
    {
        shouldClose = true;
        return CommandResult.Error;
    }


    void Run()
    {
        LoadRandNames();

        // Initialize the shell
        AddCommands();

        PrintBanner();

        string color = "White";
        int prevCount = ants.NodeCount();

        while (!shouldClose)
        {
            int deltaCount = ants.NodeCount() - prevCount;
            prevCount = ants.NodeCount();

            // Generate prompt
            if (deltaCount == 0)
            {
                Utils.PrintColored($"\n{ants.NodeCount()} ants[{color}] > ");
            }
            else
            {
                string deltaString = (deltaCount < 0 ? "[Magenta]" : "[DarkCyan]") + "(" + deltaCount.ToString("+0;-0;+0") + ")[Reset]";
                Utils.PrintColored($"\n{ants.NodeCount()} {deltaString} ants[{color}] > ");
            }

            string input = Console.ReadLine();
            CommandResult result = shell.Execute(input);

            // Set the prompt color depending of exit code of previous command
            if (result == CommandResult.Ok)
            {
                color = "White";
            }
            else
            {
                color = "Red";
            }
        }
    }

    void AddCommands()
    {
        shell.AddCommand(new CommandInfo("add", "a", "name legs", "Adds an ant to the tree", new CommandFunc(AddAntCommand)));
        shell.AddCommand(new CommandInfo("remove", "r", "name", "Removes an ant by name from the tree", new CommandFunc(RemoveAntCommand)));
        shell.AddCommand(new CommandInfo("list", "l", "[legs]", "Lists all or some ants\n" +
            "Filtering ants by predicate:\n" +
            "    n  : Exaclty n legs. Same as =n\n" +
            "    =n : Exactly n legs\n" +
            "    >n : More than n legs\n" +
            "    <n : less than n legs\n" +
            "    <= : less than or equal to n legs\n" +
            "    >= : more than or equal to n legs"
            , new CommandFunc(ListAnts)));
        shell.AddCommand(new CommandInfo("search", "s", "name", "Search for an ant", new CommandFunc(SearchAnt)));
        shell.AddCommand(new CommandInfo("clear", "c", "", "Clears the screen", new CommandFunc(ClearScreen)));
        shell.AddCommand(new CommandInfo("gen", "g", "[count=1] [minLegs=0] [maxLegs=100]", "Generates one or more random ants", new CommandFunc(GenerateAnt)));
        shell.AddCommand(new CommandInfo("purge", "p", "[count=1]", "Removes one or more ants from the beginning alphabetically.\nall or -1 removes all ants", new CommandFunc(PurgeAnt)));
        shell.AddCommand(new CommandInfo("exit", "e", "", "Exits the program", new CommandFunc(Exit)));
    }

    // Loads available random names from file
    void LoadRandNames()
    {
        FileStream file = new FileStream("RandomNames.txt", FileMode.Open);
        StreamReader stream = new StreamReader(file);

        // Read the random names file into memory
        while (true)
        {
            string line = stream.ReadLine();
            if (line == null) break;

            AddRandName(line);
        }
        file.Close();
    }

    void AddRandName(string name)
    {
        availRandNames.Add(Ant.NormalizeName(name));
    }

    // Gets a random name
    // Can return null if there are no unique random names available
    string TakeRandName()
    {
        Random rand = new Random();
        while (availRandNames.Count > 0)
        {
            // Remove random element
            int idx = rand.Next(availRandNames.Count);

            // Swap idx and last to make remove not O(N)
            string name = availRandNames[idx];
            availRandNames[idx] = availRandNames[availRandNames.Count - 1];
            availRandNames.RemoveAt(availRandNames.Count - 1);

            // Not a free name
            if (ants.Exists(name))
                continue;

            return name;
        }
        return null;
    }
}

