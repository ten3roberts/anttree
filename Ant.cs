﻿using System;

// Represents an Ant
// Can not be mutated once created to not invalidate BSTree
// Instead, mutate by replacing
struct Ant
{
    public class InvalidNameException : Exception
    {
        public InvalidNameException(string name, string reason)
            : base($"Invalid ant name '{name}'. {reason}")
        {

        }
    }

    // Trims, escapes and capitalized each word in name
    public static string NormalizeName(string name)
    {
        name = name.Trim();
        name = name.Replace("\"", string.Empty);
        name = name.Replace("'", string.Empty);

        return Utils.EscapeColored(Utils.CapitalizeWords(name));
    }

    // Returns nothing is name is valid
    // Throws a fitting exception if not
    // Valid names are no more than 10 characters long and not empty
    public static void ValidateName(string name)
    {
        if (name.Length == 0)
        {
            throw new InvalidNameException(name, "Name cannot be empty");
        }
        else if (name.Length > 10)
        {
            throw new InvalidNameException(name, "Name cannot be more than 10 characters");
        }
    }

    public Ant(string name, int legCount)
    {
        name = NormalizeName(name);
        // Throw exception if name is invalid
        ValidateName(name);

        this.name = name;
        this.legCount = legCount;
    }
    string name;
    int legCount;

    public string Name
    {
        get { return name; }
    }
    public int LegCount
    {
        get { return legCount; }
    }

    // Nicely formats an ant
    public override string ToString()
    {
        return $"'{name}' with {legCount} legs";
    }

    // Returns the legs formatted with legs in plural and singular form
    public string LegString()
    {
        return legCount + (legCount == 1 ? " leg" : " legs");
    }
}
