﻿struct CommandInfo
{
    public CommandInfo(string name, string alt, string usage, string description, CommandFunc func)
    {
        this.name = name;
        this.alt = alt;
        this.usage = usage;
        this.description = description;
        this.func = func;
    }

    // Is the same as the key
    public readonly string name;
    public readonly string alt;
    public readonly string usage;
    public readonly string description;
    public readonly CommandFunc func;
}

enum CommandResult
{
    Ok, Error, CommandNotFound, IncorrectUsage
}

delegate CommandResult CommandFunc(string[] args);

class Shell
{

    public Shell()
    {
        commands = new RBTree<string, CommandInfo>();
        altCommands = new RBTree<string, CommandInfo>();
        AddCommand(new CommandInfo("help", "h", "[command]", "Displays a list of available commands and their usage", new CommandFunc(PrintHelp)));
    }

    // Adds or overwrites a command
    public void AddCommand(CommandInfo command)
    {
        commands.Insert(command.name, command);
        if (command.alt != "")
            altCommands.Insert(command.alt, command);
    }

    // Handles and dispatches the input to the appropriate commands
    // Suggests the closest command if mispelled
    public CommandResult Execute(string input)
    {
        // Parse input and pass it to more strongly type ExecuteInternal
        input = input.Trim();
        if (input == "") return CommandResult.Ok;


        // First index is command itself
        string[] args = Utils.SplitUnquoted(input);
        string commandName = args[0].ToLower();
        return ExecuteInternal(commandName, args);
    }

    // Internal version of the execute function with the args and commandName parsed
    // Is used to rerun the command that was suggested without parsing the args again
    CommandResult ExecuteInternal(string commandName, string[] args)
    {
        CommandInfo command;
        CommandResult result = CommandResult.CommandNotFound;
        // Call the delegate and return the exit code if command was found
        if (commands.TryFind(commandName, out command))
        {
            result = command.func(args[1..]);
        }
        else if (altCommands.TryFind(commandName, out command))
        {
            result = command.func(args[1..]);
        }

        if (result == CommandResult.IncorrectUsage)
            Utils.Error($"Incorrect usage of command `{command.name}`. Use `help {command.name}` for usage");
        else if (result == CommandResult.CommandNotFound)
        {
            Utils.Error($"unknown command `{commandName}`. Use `help` for a list of commands");
            string suggestion = Utils.Suggest(commands, commandName);
            if (suggestion != null)
            {
                return ExecuteInternal(suggestion, args);
            }
        }

        return result;
    }

    // Generates help message from command with colored formatting
    public string GenHelpMessage(CommandInfo command)
    {
        string result = "[DarkCyan]" + command.name;
        if (command.alt != "")
        {
            result += "[Reset]/[Magenta]";
            result += command.alt;
        }

        result += "[Reset] " + Utils.EscapeColored(command.usage) + "\n";
        return result + "[Reset]" + Utils.Indent(4, ' ', Utils.EscapeColored(command.description)) + "\n";
    }

    // Prints the help message for one or all commands
    public CommandResult PrintHelp(string[] args)
    {
        if (args.Length != 0)
        {
            CommandInfo command;
            string commandName = args[0].ToLower();
            if (!commands.TryFind(commandName, out command) && !altCommands.TryFind(commandName, out command))
            {
                Utils.Error($"No help available for command `{commandName}`");
                return CommandResult.Error;
            }
            Utils.PrintColoredLn(GenHelpMessage(command));
        }
        else
        {
            string allCommands = "";
            foreach (var (_name, command) in commands.Traverse())
            {
                Utils.PrintColoredLn(GenHelpMessage(command));
                allCommands += (allCommands.Length == 0 ? "[DarkCyan]" : ", [DarkCyan]") + command.name + "[Reset]";
            }
            Utils.PrintColoredLn("[DarkCyan]Shows command and usage. Each argument is delimited by space.\n" +
                "Optional arguments are shown in brackets");
            Utils.PrintColoredLn("Summary: " + allCommands);
        }
        return CommandResult.Ok;
    }

    RBTree<string, CommandInfo> commands;
    // Alternative names for commands
    RBTree<string, CommandInfo> altCommands;
}
