﻿using System;
using System.Collections.Generic;

static class Utils
{
    // Represents both a foreground and background console color
    class Color
    {
        public ConsoleColor fg;
        public ConsoleColor bg;

        public Color(ConsoleColor fg, ConsoleColor bg)
        {
            this.fg = fg;
            this.bg = bg;
        }

        public Color()
        {
            fg = ConsoleColor.White;
            bg = ConsoleColor.Black;
        }

        public void SetConsoleColor()
        {
            Console.ForegroundColor = fg;
            Console.BackgroundColor = bg;
        }

        public static void Reset()
        {
            Console.ResetColor();
        }
    }

    // Prints a string to stdout
    // Every occurence of |color| will change following color
    // The color is specified by |foreground:background| or |foreground| with assumed black foreground
    // An empty color tag "||" resets the color
    // Accepted Colors are all those in System.ConsoleColor
    // Adds a newline to the end
    public static void PrintColoredLn(string msg)
    {
        PrintColored(msg + "\n");
    }

    // Prints a string with embedded color information
    // Does not include a trailing newline
    public static void PrintColored(string msg)
    {
        // Reset color before printing
        Color.Reset();
        // +1 for opening, -1 for closing
        int tagBalance = 0;
        int tagStart = 0;
        for (int i = 0; i < msg.Length; i++)
        {
            char c = msg[i];
            if (c == '[')
            {
                tagStart = i;
                tagBalance++;
            }
            else if (c == ']')
                tagBalance--;

            // Last opened bracket was closed
            if (c == ']' && tagBalance % 2 != 0)
            {
                tagStart = i;
            }
            else if (c == ']' && tagBalance % 2 == 0 && i - tagStart > 2)
            {
                // Finish the color tag and parse the color
                // Does not include the surrounding []
                string colorString = msg.Substring(tagStart + 1, i - tagStart - 1);
                Color color = ParseColor(colorString);
                if (color != null)
                    color.SetConsoleColor();
                else
                    Color.Reset();
                tagStart = i;
            }

            else if (tagBalance % 2 == 0)
            {
                Console.Write(c);
            }
        }
        Color.Reset();
    }

    // Escapes all brackets in str to prevent them being seen as a color with PrintColored
    public static string EscapeColored(string str)
    {
        string result = "";
        foreach (char c in str)
        {
            if (c == '[')
            {
                result += "[[";
            }
            else if (c == ']')
            {
                result += "]]";
            }
            else
            {
                result += c;
            }
        }
        return result;
    }

    // Attempts to parse a string "foreground:background"
    // Returns null if it could not be parsed
    static Color ParseColor(string str)
    {
        Int32 sep = str.IndexOf(':');
        Color color = new Color();
        Int32 firstPart = sep != -1 ? sep : str.Length;
        if (firstPart != 0)
        {
            if (!Enum.TryParse<ConsoleColor>(str.Substring(0, sep != -1 ? sep : str.Length), out color.fg))
                return null;
        }
        if (sep != -1)
        {
            if (!Enum.TryParse<ConsoleColor>(str.Substring(sep + 1), out color.bg))
                return null;
        }
        return color;
    }

    // Splits a string by delimiter except if it is inside quotes (' or ")
    // Does not include delimiter
    public static string[] SplitUnquoted(string str, char del = ' ')
    {
        char quotes = '\0';
        int beg = 0;
        List<string> parts = new List<string>();

        for (int i = 0; i < str.Length; i++)
        {
            char c = str[i];
            if (c == '\'' || c == '"')
                // Matching and closing quote
                if (quotes == c)
                {
                    quotes = '\0';
                    continue;
                }
                // Opening quote
                else if (quotes == '\0')
                {
                    quotes = c;
                    continue;
                }
                // Not a matching quote but a quote character
                else { }

            // Split
            if (quotes == '\0' && c == del)
            {
                parts.Add(str[beg..i]);
                // Skip part and delimiter
                beg = i + 1;
            }
        }

        // Add remaining part if any
        if (beg != str.Length)
        {
            parts.Add(str[beg..]);
        }

        return parts.ToArray();
    }

    // Prints an error in red
    public static void Error(string msg)
    {
        Utils.PrintColoredLn("[Red] Error: " + msg);
    }

    public static string Indent(int width, char c, string str)
    {
        string sep = "\n" + new string(c, width);
        string[] lines = str.Split('\n');
        return new string(c, width) + string.Join(sep, lines);
    }

    // Makes the first char in str capitalized and the rest uncapitalized
    public static string CapitalizeFirst(string str)
    {
        return str[0..1].ToUpper() + str[1..].ToLower();
    }

    // Applies CapitalizeFirst to each word delimited by space not in quotes
    public static string CapitalizeWords(string str)
    {
        string[] words = SplitUnquoted(str);
        for (int i = 0; i < words.Length; i++)
        {
            words[i] = CapitalizeFirst(words[i]);
        }
        return string.Join(' ', words);
    }

    // Suggests the closest match to input from provided RBTree
    // Prompts the user and returns either the suggestion or null
    public static string Suggest<V>(RBTree<string, V> tree, string input, bool defaultYes = true)
    {
        string closest = RBTree<string, V>.FindClosest(tree, input);

        // No closest found or it is too long for the input
        if (closest == null || Math.Abs(closest.Length - input.Length) > 5)
            return null;

        Utils.PrintColored($"[White]Did you mean [Cyan]{closest}[White]? " + (defaultYes ? "[[[DarkGreen]Y[Reset]/n]]" : "[[y/[DarkRed]N[Reset]]]"));
        string prompt = Console.ReadLine();

        if ((defaultYes && prompt == "") || prompt.StartsWith('y'))
            return closest;

        return null;
    }
}